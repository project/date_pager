<?php

namespace Drupal\date_pager\Plugin\views\pager;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\pager\PagerPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\date_pager\PagerDate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Date pager plugin implementation.
 *
 * Lets you choose a date field from the view's table
 * and page it with a flexible granularity.
 *
 * @ingroup views_pager_plugins
 *
 * @ViewsPager(
 *   id = "date",
 *   title = @Translation("Date Pager"),
 *   short_title = @Translation("Date"),
 *   theme = "datepager",
 *   register_theme = TRUE,
 *   help = @Translation("Page by Date and choose a granularity for a date field")
 * )
 */
class DatePager extends PagerPluginBase implements CacheableDependencyInterface {

  /**
   * Supported date field types.
   *
   * @var string[]
   */
  protected $supportedDateTypes = [
    'changed',
    'created',
    'datetime',
    'daterange',
    'smartdate',
  ];

  /**
   * View's base entity.
   *
   * @var string
   */
  protected $baseEntityTypeId;

  /**
   * The earliest DateTime available.
   *
   * @var null|string
   */
  public $minDate = NULL;

  /**
   * The latest DateTime available.
   *
   * @var null|string
   */
  public $maxDate = NULL;

  /**
   * The currently active DateTime.
   *
   * @var \Drupal\date_pager\PagerDate
   */
  public $activeDate;

  /**
   * Date format.
   *
   * @var string
   */
  public $dateFormatString;

  /**
   * Type of the date field.
   *
   * @var string
   */
  protected $dateFieldType = 'datetime';

  /**
   * Table field name of the date.
   *
   * @var string
   */
  protected $fieldTable = 'field_date';

  /**
   * Table column of the date.
   *
   * @var string
   */
  protected $startDateColumn = 'value';

  /**
   * Table column of the end date.
   *
   * @var null|string
   */
  protected $endDateColumn = NULL;

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityFieldManager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->baseEntityTypeId = $view->getBaseEntityType()->id();
    // Sets date format String.
    $this->dateFormatString = $this->getFormatString($this->options['granularity']);
    // Get the field type to check for smartdate field.
    if (!empty($this->options['date_field'])) {

      // Workaround for backwards compatibility.
      $parts = explode('.', $this->options['date_field']);
      if (stripos($parts[0], '__')) {
        $this->options['date_field'] = implode('.', explode('__', $parts[0]));
      }
      [$entity_type, $field_name] = explode('.', $this->options['date_field']);
      $table_mapping = $this->entityTypeManager->getStorage($entity_type)
        ->getTableMapping();
      $field_storage = $this->entityFieldManager
        ->getFieldStorageDefinitions($entity_type)[$field_name];
      $columns = $table_mapping->getColumnNames($field_name);
      if ($field_storage) {
        $this->dateFieldType = $field_storage->getType();
        $this->startDateColumn = $columns['value'];
        if (isset($columns['end_value'])) {
          $this->endDateColumn = $columns['end_value'];
        }
        if ($field_storage->isBaseField()) {
          $this->fieldTable = $this->entityTypeManager->getStorage($entity_type)
            ->getDataTable();
        }
        else {
          $this->fieldTable = $table_mapping->getDedicatedDataTableName($field_storage);
        }
      }
    }

  }

  /**
   * Helper function for date format.
   *
   * @param int $granularity
   *   Numerical expression of desired granularity.
   *
   * @return string
   *   DateTime date format string part w/ granularity.
   */
  private function getFormatString($granularity) {
    $date_parts = ['Y', '-m', '-d', '\TH', ':i'];
    return implode('', array_slice($date_parts, 0, $granularity + 1));
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['granularity'] = ['default' => '3'];
    $options['default_page'] = ['default' => 'now'];
    $options['date_field'] = ['default' => FALSE];
    $options['date_sort'] = ['default' => FALSE];

    return $options;
  }

  /**
   * Provide the default form for setting options.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Select the granularity of the pager.
    $form['granularity'] = [
      '#type' => 'select',
      '#title' => $this->t('Granularity'),
      '#description' => $this->t("Select the maximum granularity of the pager"),
      '#options' => [
        0 => $this->t('Year'),
        1 => $this->t('Month'),
        2 => $this->t('Day'),
        3 => $this->t('Hour'),
        4 => $this->t('Minute'),
      ],
      '#default_value' => $this->options['granularity'],
    ];

    // Time to be displayed if no page was selected.
    $form['default_page'] = [
      '#type' => 'select',
      '#title' => $this->t('Default time'),
      '#description' => $this->t("Time to be displayed if no page was selected"),
      '#options' => [
        'earliest' => $this->t('earliest'),
        'now' => $this->t('current'),
        'latest' => $this->t('latest'),
      ],
      '#default_value' => $this->options['default_page'],
    ];

    // Date field to page on.
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Date field'),
      '#description' => $this->t("Select the Date field to page on"),
      '#options' => $this->getDateFieldOptions(),
      '#default_value' => $this->options['date_field'],
    ];

    // Reverse order.
    $form['date_sort'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reverse order'),
      '#description' => $this->t("Sort the dates in descending order"),
      '#default_value' => $this->options['date_sort'],
    ];
  }

  /**
   * Lists all the date fields from fields and entity keys.
   *
   * @return array
   *   List of available DateTime fields.
   */
  private function getDateFieldOptions() {
    // Available Date field options.
    $date_fields = [];

    // View's entities field and storage mappings.
    $base_entity_type_id = $this->view->getBaseEntityType()->id();
    $storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($base_entity_type_id);

    // List all possible fields.
    foreach ($this->supportedDateTypes as $field_type) {
      $field_map = $this->entityFieldManager->getFieldMapByFieldType($field_type);
      if (isset($field_map[$base_entity_type_id])) {
        foreach ($field_map[$base_entity_type_id] as $field_name => $field) {
          if ($storage_definitions[$field_name]) {
            $identifier = "{$base_entity_type_id}.{$field_name}";
            $date_fields[$identifier] = "";
            $all_fields = [];
            foreach ($field['bundles'] as $bundle) {
              $bundle_fields = $this->entityFieldManager->getFieldDefinitions($base_entity_type_id, $bundle);
              // This can get very long.
              // @todo Find a nicer way to list a lot of bundles.
              $all_fields[] = $bundle_fields[$field_name]->getLabel() . " ($bundle)";
            }
            $date_fields[$identifier] = implode(', ', $all_fields);
          }
        }
      }

    }
    return $date_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {

    // Check granularity values.
    $granularity = $form_state->getValue(['pager_options', 'granularity']);
    if (!is_numeric($granularity) || intval($granularity) < 0 || intval($granularity) > 4) {
      $form_state->setErrorByName('pager_options][granularity', $this->t('Please select a valid granularity.'));
    }

    // Check date_field value.
    $date_field = $form_state->getValue(['pager_options', 'date_field']);
    if (empty($date_field)) {
      // @todo Check if valid date field.
      $form_state->setErrorByName('pager_options][date_field', $this->t('Please select a valid date field.'));
    }
  }

  /**
   * Adds a date pager query to the field.
   */
  public function query() {
    $table_alias = $this->fieldTable;
    $value_column = $this->startDateColumn;
    $this->view->query->addField($table_alias, $value_column, $value_column);
    if (!is_null($this->endDateColumn)) {
      $end_column = $this->endDateColumn;
      $this->view->query->addField($table_alias, $end_column, $end_column);
    }

    // Checks if the date field is used, e.g. in a contextual filter.
    $found = FALSE;
    if (isset($this->view->query->where[0]['conditions'])) {
      foreach ($this->view->query->where[0]['conditions'] as &$condition) {
        if ($condition['field'] == $value_column) {
          $condition['value'] = $this->activeDate . '%';
          $condition['operator'] = 'LIKE';
          $found = TRUE;
        }
      }
    }

    // If the date field is not already used, add a new where query.
    if (!$found) {
      if ($this->dateFieldType === 'datetime') {
        $this->view->query->addWhere(1, $value_column, $this->activeDate . '%', 'LIKE');
      }
      // If we have a (smart) date range field where start and end
      // overlaps selected date.
      else {
        $timestamp = $this->activeDate->format('U');
        switch ($this->options['granularity']) {
          case 0:
            $start_date = date('Y-01-01 00:00:00', $timestamp);
            $end_date = date('Y-12-31 23:59:59', $timestamp);
            break;

          case 1:
            $start_date = date('Y-m-01 00:00:00', $timestamp);
            $end_date = date('Y-m-t 23:59:59', $timestamp);
            break;

          case 2:
            $start_date = date('Y-m-d 00:00:00', $timestamp);
            $end_date = date('Y-m-d 23:59:59', $timestamp);
            break;

          case 3:
            $start_date = date('Y-m-d H:00:00', $timestamp);
            $end_date = date('Y-m-d H:59:59', $timestamp);
            break;

          default:
            $start_date = date('Y-m-d H:i:00', $timestamp);
            $end_date = date('Y-m-d H:i:59', $timestamp);
            break;
        }
        // With smartdate timestamps need to be converted to datetime.
        if (in_array($this->dateFieldType, ['created', 'changed', 'smartdate'])) {
          $start_date = strtotime($start_date);
          $end_date = strtotime($end_date);
        }
        $this->view->query->addWhere(1, $value_column, $end_date, '<=');
        if (in_array($this->dateFieldType, ['created', 'changed'])) {
          $this->view->query->addWhere(1, $value_column, $start_date, '>=');
        }
        else {
          $this->view->query->addWhere(1, $end_column, $start_date, '>=');
        }
      }
    }
  }

  /**
   * Get field date range of min max values.
   *
   * @return array
   *   Min and max values from the query.
   */
  private function getDateRange() {
    $end_column = is_null($this->endDateColumn) ? $this->startDateColumn : $this->endDateColumn;
    $query = $this->database->select($this->fieldTable);
    $query->addExpression('MIN(' . $this->startDateColumn . ')', 'min');
    $query->addExpression('MAX(' . $end_column . ')', 'max');
    return $query->execute()->fetchAssoc();
  }

  /**
   * Sets the current page date.
   *
   * @param string $time
   *   If provided, the active date will be set to the timestamp.
   */
  public function setCurrentPage($time = NULL) {

    // Get min and max values for the selected field.
    $range = $this->getDateRange();

    $min_date = $range['min'];
    $max_date = $range['max'];

    // With smartdate timestamps need to be converted to datetime.
    if (in_array($this->dateFieldType, ['smartdate', 'created', 'changed'])) {
      $min_date = date($this->dateFormatString, $range['min']);
      $max_date = date($this->dateFormatString, $range['max']);
    }

    $this->minDate = new PagerDate($min_date, $this->dateFormatString);
    $this->maxDate = new PagerDate($max_date, $this->dateFormatString);

    // Try to get activeDate from URL Parameter.
    $date_parameter = $this->view->getRequest()->query->get('date');
    if (isset($date_parameter) && preg_match("/^\d{4}(-[0-1][0-9](-[0-3][0-9](T[0-2][0-9](:[0-6][0-9])?)?)?)?$/", $date_parameter)) {
      $this->activeDate = new PagerDate($date_parameter);
    }
    else {
      switch ($this->options['default_page']) {
        case 'earliest':
          $time = $this->minDate;
          break;

        case 'latest':
          $time = $this->maxDate;
          break;

        default:
          $time = date($this->dateFormatString);
          $time = new PagerDate($time, $this->dateFormatString);
          break;
      }
      $this->activeDate = $time;
    }
  }

  /**
   * Returns a string to display as the clickable title for the pager plugin.
   *
   * @return string
   *   Title.
   */
  public function summaryTitle() {
    return $this->t('Page by date');
  }

  /**
   * {@inheritdoc}
   */
  public function usePager() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function useCountQuery() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function usesExposed() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // The rendered link needs to play well with any other query parameter used
    // on the page, like other pagers and exposed filter.
    return ['url.query_args'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    return [
      '#theme' => $this->themeFunctions(),
      '#parameters' => [
        'min' => $this->minDate,
        'max' => $this->maxDate,
        'current' => $this->activeDate,
        'format' => $this->activeDate->granularity,
        'current_granularity' => $this->activeDate->granularityId,
        'route_name' => !empty($this->view->live_preview) ? '<current>' : '<none>',
      ],
      '#options' => $this->options,
    ];
  }

}
