<?php

namespace Drupal\Tests\date_pager\Functional;

use Drupal\date_pager\PagerDate;
use Drupal\Tests\UnitTestCase;

/**
 * Tests that the condition plugins work.
 *
 * @group date_pager
 */
class PagerDateUtcUnitTest extends UnitTestCase {

  /**
   * Test pager date.
   */
  public function testPagerDateUtc(): void {
    date_default_timezone_set('UTC');

    $date1 = '2022-01-01T00:00:00';
    $pager_date1 = new PagerDate('2022');
    $this->assertEquals($date1, $pager_date1->format('Y-m-d\TH:i:s'));
    $this->assertEquals('2022', $pager_date1->format($pager_date1->granularity));
    $this->assertEquals(0, $pager_date1->granularityId);
    $this->assertEquals('Y', $pager_date1->granularity);

    $date2 = '2023-01-31T22:11:05';
    $pager_date2 = new PagerDate($date2);
    $this->assertEquals($date2, $pager_date2->format($pager_date2->granularity));
    $this->assertEquals(5, $pager_date2->granularityId);
    $this->assertEquals('Y-m-d\TH:i:s', $pager_date2->granularity);
    $this->assertEquals(strtotime($date2 . ' UTC'), $pager_date2->toTime());

    $date3 = '2021-02-13T15:09:04';
    $pager_date3 = new PagerDate($date3, 'Y-m');
    $this->assertEquals('2021-02', $pager_date3->format($pager_date3->granularity));
    $this->assertEquals(1, $pager_date3->granularityId);
    $this->assertEquals('Y-m', $pager_date3->granularity);
    // First between the date two and three.
    $this->assertTrue($pager_date1->between($pager_date3, $pager_date2));
  }

}
