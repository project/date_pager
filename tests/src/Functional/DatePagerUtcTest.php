<?php

namespace Drupal\Tests\date_pager\Functional;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the condition plugins work.
 *
 * @group date_pager
 */
class DatePagerUtcTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'date_pager',
    'datetime',
    'datetime_range',
    'field_ui',
    'node',
    'smart_date',
    'views_ui',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the minimal profile.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $admin;

  /**
   * Datetime1.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus
   */
  protected $dateTime1;

  /**
   * Datetime fieldname.
   *
   * @var string
   */
  protected $datetimeFieldName;

  /**
   * Daterange1.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus[]
   */
  protected $dateRange1;

  /**
   * Node1.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodePage1;

  /**
   * Datetime2.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus
   */
  protected $dateTime2;

  /**
   * Daterange fieldname.
   *
   * @var string
   */
  protected $daterangeFieldName;

  /**
   * Smartdate fieldname.
   *
   * @var string
   */
  protected $smartdateFieldName;

  /**
   * Created fieldname.
   *
   * @var string
   */
  protected $createdDateFieldName = 'created';

  /**
   * Daterange2.
   *
   * @var \Drupal\Component\Datetime\DateTimePlus[]
   */
  protected $dateRange2;

  /**
   * Node2.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodePage2;

  /**
   * Timezone.
   *
   * @var string
   */
  protected $timeZone = 'UTC';

  /**
   * Time format.
   *
   * @var string
   */
  protected $timeFormat = 'Y-m-d\\TH:i:s';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    date_default_timezone_set($this->timeZone);
    $this->admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);

    // Timezone settings.
    $this->drupalGet('user/' . $this->admin->id() . '/edit');
    $this->submitForm([
      'timezone' => $this->timeZone,
    ], 'Save');
    $this->drupalGet('admin/config/regional/settings');
    $this->submitForm([
      'date_default_timezone' => $this->timeZone,
    ], 'Save');

  }

  /**
   * Test date pager.
   */
  public function testDatePager(): void {

    // Add datetime field.
    $this->datetimeFieldName = 'testfield_datetime';
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $this->datetimeFieldName,
      'entity_type' => 'node',
      'type' => 'datetime',
      'settings' => ['datetime_type' => 'datetime_default'],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'label' => $this->datetimeFieldName,
      'bundle' => 'page',
      'description' => 'Description for ' . $this->datetimeFieldName,
      'required' => TRUE,
    ]);
    $field->save();

    // Add daterange field.
    $this->daterangeFieldName = 'testfield_daterange';
    $fieldRangeStorage = FieldStorageConfig::create([
      'field_name' => $this->daterangeFieldName,
      'entity_type' => 'node',
      'type' => 'daterange',
      'settings' => ['datetime_type' => 'datetime_default'],
    ]);
    $fieldRangeStorage->save();
    $fieldRange = FieldConfig::create([
      'field_storage' => $fieldRangeStorage,
      'label' => $this->daterangeFieldName,
      'bundle' => 'page',
      'description' => 'Description for ' . $this->daterangeFieldName,
      'required' => TRUE,
    ]);
    $fieldRange->save();

    // Add smartdate field.
    $this->smartdateFieldName = 'testfield_smartdate';
    $fieldSmartStorage = FieldStorageConfig::create([
      'field_name' => $this->smartdateFieldName,
      'entity_type' => 'node',
      'type' => 'smartdate',
    ]);
    $fieldSmartStorage->save();
    $fieldSmart = FieldConfig::create([
      'field_storage' => $fieldSmartStorage,
      'label' => $this->smartdateFieldName,
      'bundle' => 'page',
      'description' => 'Description for ' . $this->smartdateFieldName,
      'required' => TRUE,
    ]);
    $fieldSmart->setDefaultValue([
      'default_duration' => 60,
      'default_duration_increments' => "30\r\n60|1 hour\r\n90\r\n120|2 hours\r\ncustom",
      'default_date_type' => '',
      'default_date' => '',
      'min' => '',
      'max' => '',
    ]);
    $fieldSmart->save();

    // Set up display Datetime.
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'default',
      'format_type' => 'short',
    ];
    $this->container->get('entity_display.repository')
      ->getFormDisplay($field->getTargetEntityTypeId(), $field->getTargetBundle())
      ->setComponent($this->datetimeFieldName, $displayOptions)
      ->save();
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'datetime_default',
      'format_type' => 'short',
      'timezone_override' => $this->timeZone,
    ];
    $this->container->get('entity_display.repository')
      ->getViewDisplay($field->getTargetEntityTypeId(), $field->getTargetBundle())
      ->setComponent($this->datetimeFieldName, $displayOptions)
      ->save();

    // Set up display Daterange.
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'daterange_default',
    ];
    $this->container->get('entity_display.repository')
      ->getFormDisplay($fieldRange->getTargetEntityTypeId(), $fieldRange->getTargetBundle())
      ->setComponent($this->daterangeFieldName, $displayOptions)
      ->save();
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'daterange_default',
      'format_type' => 'short',
      'timezone_override' => $this->timeZone,
      'separator' => ' # ',
    ];
    $this->container->get('entity_display.repository')
      ->getViewDisplay($fieldRange->getTargetEntityTypeId(), $fieldRange->getTargetBundle())
      ->setComponent($this->daterangeFieldName, $displayOptions)
      ->save();

    // Set up display Smartdate.
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'smartdate_inline',
    ];
    $this->container->get('entity_display.repository')
      ->getFormDisplay($fieldSmart->getTargetEntityTypeId(), $fieldSmart->getTargetBundle())
      ->setComponent($this->smartdateFieldName, $displayOptions)
      ->save();
    $displayOptions['region'] = 'content';
    $displayOptions['settings'] = [
      'type' => 'smartdate_default',
      'format' => 'default',
      'timezone_override' => $this->timeZone,
    ];
    $this->container->get('entity_display.repository')
      ->getViewDisplay($fieldSmart->getTargetEntityTypeId(), $fieldSmart->getTargetBundle())
      ->setComponent($this->smartdateFieldName, $displayOptions)
      ->save();

    // Create Nodes.
    $this->dateTime1 = DateTimePlus::createFromFormat($this->timeFormat, '2010-02-01T10:09:00', 'UTC');
    $this->dateRange1[0] = DateTimePlus::createFromFormat($this->timeFormat, '2006-02-01T10:09:00', 'UTC');
    $this->dateRange1[1] = DateTimePlus::createFromFormat($this->timeFormat, '2006-02-02T10:09:00', 'UTC');
    $this->dateRange1[2] = DateTimePlus::createFromFormat($this->timeFormat, '2001-02-01T10:09:00', 'UTC');
    $this->dateRange1[3] = DateTimePlus::createFromFormat($this->timeFormat, '2001-02-02T11:09:00', 'UTC');
    $this->nodePage1 = $this->createNode([
      'title' => 'Page 1',
      'type' => 'page',
      $this->datetimeFieldName => $this->dateTime1->format($this->timeFormat),
      $this->daterangeFieldName => [
        'value' => $this->dateRange1[0]->format($this->timeFormat),
        'end_value' => $this->dateRange1[1]->format($this->timeFormat),
      ],
      $this->smartdateFieldName => [
        'value' => $this->dateRange1[2]->format('U'),
        'end_value' => $this->dateRange1[3]->format('U'),
      ],
    ]);

    $this->drupalGet('node/' . $this->nodePage1->id() . '/edit');
    $this->dateTime2 = DateTimePlus::createFromFormat($this->timeFormat, '2020-01-01T00:08:00', 'UTC');
    $this->dateRange2[0] = DateTimePlus::createFromFormat($this->timeFormat, '2009-02-01T09:09:00', 'UTC');
    $this->dateRange2[1] = DateTimePlus::createFromFormat($this->timeFormat, '2009-02-01T10:09:00', 'UTC');
    $this->dateRange2[2] = DateTimePlus::createFromFormat($this->timeFormat, '2004-02-01T10:09:00', 'UTC');
    $this->dateRange2[3] = DateTimePlus::createFromFormat($this->timeFormat, '2004-02-01T11:09:00', 'UTC');
    $this->nodePage2 = $this->createNode([
      'title' => 'Page 2',
      'type' => 'page',
      $this->datetimeFieldName => $this->dateTime2->format($this->timeFormat),
      $this->daterangeFieldName => [
        'value' => $this->dateRange2[0]->format($this->timeFormat),
        'end_value' => $this->dateRange2[1]->format($this->timeFormat),
      ],
      $this->smartdateFieldName => [
        'value' => $this->dateRange2[2]->format('U'),
        'end_value' => $this->dateRange2[3]->format('U'),
      ],
    ]);
    $this->drupalGet('node/' . $this->nodePage1->id());
    $this->assertSession()->pageTextContains($this->dateTime1->format('j M Y - H:i', ['timezone' => $this->timeZone]));
    $this->assertSession()->pageTextContains($this->dateRange1[0]->format('j M Y - H:i', ['timezone' => $this->timeZone]) . ' # ' . $this->dateRange1[1]->format('j M Y - H:i', ['timezone' => $this->timeZone]));
    $this->assertSession()->pageTextContains($this->dateRange1[2]->format('D, M j Y, g:ia', ['timezone' => $this->timeZone]) . ' - ' . $this->dateRange1[3]->format('D, M j Y, g:ia', ['timezone' => $this->timeZone]));
    $this->drupalGet('node/' . $this->nodePage2->id());
    $this->assertSession()->pageTextContains($this->dateTime2->format('j M Y - H:i', ['timezone' => $this->timeZone]));
    $this->assertSession()->pageTextContains($this->dateRange2[0]->format('j M Y - H:i', ['timezone' => $this->timeZone]) . ' # ' . $this->dateRange2[1]->format('j M Y - H:i', ['timezone' => $this->timeZone]));
    $this->assertSession()->pageTextContains($this->dateRange2[2]->format('D, M j Y, g:i', ['timezone' => $this->timeZone]) . ' - ' . $this->dateRange2[3]->format('g:ia', ['timezone' => $this->timeZone]));

    // Set up view.
    $this->drupalGet('admin/structure/views/add');
    $this->submitForm([
      'label' => 'Testview',
      'id' => 'testview',
      'page[create]' => TRUE,
      'page[title]' => 'Testview',
      'page[path]' => 'testview',
      'page[style][style_plugin]' => 'html_list',
      'page[style][row_plugin]' => 'full_posts',
    ], 'Save and edit');
    $this->drupalGet('admin/structure/views/nojs/display/testview/page_1/pager');
    $this->submitForm([
      'pager[type]' => 'date',
    ], 'Apply');
    $this->submitForm([
      'pager_options[granularity]' => 2,
      'pager_options[date_field]' => 'node.' . $this->datetimeFieldName,
    ], 'Apply');
    $this->submitForm([], 'Save');

    $this->datetimeFieldPager();
    $this->dateRangeFieldPager();
    $this->smartDateFieldPager();
    $this->createdDateFieldPager();
  }

  /**
   * Datetime sub test.
   */
  public function datetimeFieldPager() {
    // Test date time field pager.
    $this->drupalGet('admin/structure/views/nojs/display/testview/page_1/pager_options');
    $this->submitForm([
      'pager_options[granularity]' => 2,
      'pager_options[date_field]' => 'node.' . $this->datetimeFieldName,
    ], 'Apply');
    $this->drupalGet('admin/structure/views/view/testview');
    $this->submitForm([], 'Save');

    $this->drupalGet('testview');
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateTime1->format('Y', ['timezone' => $this->timeZone]));
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateTime2->format('Y', ['timezone' => $this->timeZone]));
    $this->assertSession()->linkByHrefNotExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateTime1->format('Y', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateTime1->format('Y-m-d', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateTime2->format('Y', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateTime2->format('Y-m-d\\TH', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());
  }

  /**
   * Daterange sub test.
   */
  public function dateRangeFieldPager() {
    // Test date range field pager.
    $this->drupalGet('admin/structure/views/nojs/display/testview/page_1/pager_options');
    $this->submitForm([
      'pager_options[granularity]' => 1,
      'pager_options[date_field]' => 'node.' . $this->daterangeFieldName,
    ], 'Apply');
    $this->drupalGet('admin/structure/views/view/testview/edit/page_1');
    $this->submitForm([], 'Save');

    $this->drupalGet('testview');
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateRange1[0]->format('Y', ['timezone' => $this->timeZone]));
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateRange2[0]->format('Y', ['timezone' => $this->timeZone]));
    $this->assertSession()->linkByHrefNotExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange1[0]->format('Y-m', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange1[0]->format('Y-m-d\\TH', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange2[0]->format('Y-m', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange2[0]->format('Y-m-d\\TH', ['timezone' => $this->timeZone])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());
  }

  /**
   * Smartdate sub test.
   */
  public function smartDateFieldPager() {
    // Test smart date field pager.
    $this->drupalGet('admin/structure/views/nojs/display/testview/page_1/pager_options');
    $this->submitForm([
      'pager_options[granularity]' => 2,
      'pager_options[date_field]' => 'node.' . $this->smartdateFieldName,
    ], 'Apply');
    $this->drupalGet('admin/structure/views/view/testview/edit/page_1');
    $this->submitForm([], 'Save');

    $this->drupalGet('testview');
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateRange1[2]->format('Y', ['timezone' => 'UTC']));
    $this->assertSession()->linkByHrefExists('?date=' . $this->dateRange2[2]->format('Y', ['timezone' => 'UTC']));
    $this->assertSession()->linkByHrefNotExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange1[2]->format('Y-m', ['timezone' => 'UTC'])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange1[2]->format('Y-m-d\\TH', ['timezone' => 'UTC'])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange2[2]->format('Y-m', ['timezone' => 'UTC'])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());

    $this->drupalGet('testview', ['query' => ['date' => $this->dateRange2[2]->format('Y-m-d\\TH', ['timezone' => 'UTC'])]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());
  }

  /**
   * Created date sub test.
   */
  public function createdDateFieldPager() {
    // Test smart date field pager.
    $this->drupalGet('admin/structure/views/nojs/display/testview/page_1/pager_options');
    $this->submitForm([
      'pager_options[granularity]' => 0,
      'pager_options[date_field]' => 'node.' . $this->createdDateFieldName,
    ], 'Apply');
    $this->drupalGet('admin/structure/views/view/testview/edit/page_1');
    $this->submitForm([], 'Save');

    $this->drupalGet('testview');
    $this->drupalGet('testview', ['query' => ['date' => (intval(date('Y')) - 1)]]);
    $this->assertSession()->linkByHrefExists('?date=' . date('Y'));
    $this->assertSession()->linkByHrefNotExists('node/' . $this->nodePage1->id());
    $this->assertSession()->linkByHrefNotExists('node/' . $this->nodePage2->id());

    $this->drupalGet('testview', ['query' => ['date' => date('Y')]]);
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage1->id());
    $this->assertSession()->linkByHrefExists('node/' . $this->nodePage2->id());

  }

}
